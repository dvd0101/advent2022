#include <array>
#include <fstream>
#include <iostream>

enum shape {
    rock = 1,
    paper = 2,
    scissor = 3,
};

enum outcome {
    lost = 0,
    draw = 3,
    won = 6,
};

const std::array<shape[2], 3> p1_loser = {{
    {rock, paper},
    {paper, scissor},
    {scissor, rock},
}};

outcome round(shape player1, shape player2) {
    if (player1 == player2)
        return draw;

    for (auto [s1, s2] : p1_loser) {
        if (player1 == s1 && player2 == s2)
            return lost;
    }

    return won;
}

shape choose(shape player1, outcome result) {
    if (result == lost) {
        for (auto [s1, s2] : p1_loser) {
            if (player1 == s2)
                return s1;
        }
    } else if (result == won) {
        for (auto [s1, s2] : p1_loser) {
            if (player1 == s1)
                return s2;
        }
    }

    return player1;
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day2a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);

    int64_t score1 = 0;
    int64_t score2 = 0;

    while (true) {
        char a, b;
        in >> a >> b;
        if (!in)
            break;

        shape player1 = static_cast<shape>(a - 'A' + 1);
        shape player2 = static_cast<shape>(b - 'X' + 1);
        auto result = round(player2, player1);

        score1 += result + static_cast<int>(player2);

        result = static_cast<outcome>((b - 'X') * 3);
        player2 = choose(player1, result);

        score2 += result + static_cast<int>(player2);
    }

    std::cout << score1 << "\n";
    std::cout << score2 << "\n";
}
