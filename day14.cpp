#include <climits>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <optional>
#include <ostream>
#include <string>
#include <unordered_map>
#include <vector>

#include "./geo.h"

struct world {
    std::unordered_map<utils::pos, char> cells;
    utils::pos tl = {INT_MAX, INT_MAX};
    utils::pos br = {INT_MIN, INT_MIN};

    void add(utils::pos pt, char c) {
        cells[pt] = c;

        tl.x = std::min(tl.x, pt.x);
        tl.y = std::min(tl.y, pt.y);
        br.x = std::max(br.x, pt.x);
        br.y = std::max(br.y, pt.y);
    }

    std::optional<utils::dir> can_move(utils::pos pt, std::optional<int> floor_y = {}) const {
        using utils::dir;
        auto test = [&](dir d) {
            if (floor_y.has_value() && (pt + d).y >= *floor_y) {
                return false;
            }
            auto it = cells.find(pt + d);
            return it == cells.end();
        };

        dir d = {.dx = 0, .dy = 1};
        if (test(d))
            return d;

        d.dx = -1;
        if (test(d))
            return d;

        d.dx = 1;
        if (test(d))
            return d;

        return {};
    }
};

void init_world(std::istream& in, world& w) {
    using utils::dir;
    using utils::pos;

    int start = 0;
    pos p1;
    while (true) {
        pos p2;
        char ch;
        in >> p2.x >> ch >> p2.y;
        if (!in)
            break;

        if (start == 0) {
            start = 1;
        } else {
            dir d{
                .dx = p2.x - p1.x,
                .dy = p2.y - p1.y,
            };
            if (d.dx)
                d.dx /= std::abs(d.dx);
            if (d.dy)
                d.dy /= std::abs(d.dy);
            while (p1 != p2) {
                w.add(p1, '#');
                p1 = p1 + d;
            }
            w.add(p2, '#');
        }
        p1 = p2;
        if (in.peek() == ' ') {
            in.ignore(4);
        } else {
            assert(in.peek() == '\n');
            start = 0;
        }
    }

    w.add({500, 0}, '+');
}

std::ostream& operator<<(std::ostream& os, const world& w) {
    os << w.tl << " " << w.br << "\n";
    for (int y = w.tl.y; y <= w.br.y; y++) {
        os << "| ";
        os << std::setw(3) << y << std::setw(0) << " ";
        for (int x = w.tl.x; x <= w.br.x; x++) {
            auto it = w.cells.find(utils::pos{x, y});
            char ch = it == w.cells.end() ? '.' : it->second;
            os << ch;
        }
        os << " |\n";
    }
    return os;
}

bool simulate(world& w) {
    utils::pos pt{500, 0};
    while (true) {
        auto d = w.can_move(pt);
        if (!d.has_value())
            break;

        pt = pt + *d;
        if (pt.y >= w.br.y) {
            return true;
        }
    }
    w.add(pt, 'o');
    return false;
}

bool simulate(world& w, int floor_y) {
    utils::pos src{500, 0};
    utils::pos pt = src;
    do {
        auto d = w.can_move(pt, floor_y);
        if (!d.has_value()) {
            break;
        }

        pt = pt + *d;
    } while (pt != src);
    w.add(pt, 'o');
    return pt == src;
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day14a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);
    world w;

    init_world(in, w);

    auto w0 = w;
    int p1 = 0;
    while (!simulate(w0))
        p1++;
    std::cout << w0 << "\n";

    int p2 = 1;
    int floor = w.br.y + 2;
    while (!simulate(w, floor))
        p2++;

    std::cout << p1 << "\n";
    std::cout << p2 << "\n";
}
