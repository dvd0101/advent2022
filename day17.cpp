#include <cassert>
#include <fstream>
#include <iostream>
#include <string>

#include "./geo.h"

struct movements {
    size_t ix = 0;
    std::string data;

    char next() {
        char ch = data[ix];
        ix = (ix + 1) % data.size();
        return ch;
    }
};

utils::grid<char> shape1() {
    return {
        .cells = {'#', '#', '#', '#'},
        .width = 4,
        .height = 1,
    };
}

utils::grid<char> shape2() {
    return {
        // clang-format off
        .cells = {
            ' ', '#', ' ',
            '#', '#', '#',
            ' ', '#', ' ',
        },
        // clang-format on
        .width = 3,
        .height = 3,
    };
}

utils::grid<char> shape3() {
    return {
        // clang-format off
        .cells = {
            ' ', ' ', '#',
            ' ', ' ', '#',
            '#', '#', '#',
        },
        // clang-format on
        .width = 3,
        .height = 3,
    };
}

utils::grid<char> shape4() {
    return {
        // clang-format off
        .cells = {
            '#',
            '#',
            '#',
            '#',
        },
        // clang-format on
        .width = 1,
        .height = 4,
    };
}

utils::grid<char> shape5() {
    return {
        // clang-format off
        .cells = {
            '#', '#',
            '#', '#',
        },
        // clang-format on
        .width = 2,
        .height = 2,
    };
}

const utils::grid<char> shapes[5] = {
    shape1(),
    shape2(),
    shape3(),
    shape4(),
    shape5(),
};

// a piece is a shape placed on the space; offest is the coordinates of the
// shape top-left corner.
struct piece {
    size_t shape_index;
    utils::pos offset;
};

struct chamber {
    const int width = 7;
    utils::grid<char> space;
};

bool collision(const utils::grid<char>& a, utils::pos offset, const utils::grid<char>& b) {
    for (size_t i = 0; i < b.cells.size(); i++) {
        if (b.cells[i] == ' ')
            continue;

        auto p = b.indexToPos(i) + offset;
        p.y = std::abs(p.y - (b.height - 1));
        auto elem = a.lookup(p);

        // `p` is outside `a`, so no collision
        if (elem == nullptr)
            continue;

        if (*elem == '#')
            return true;
    }
    return false;
}

void paste(chamber& ch, utils::pos offset, const utils::grid<char>& src) {
    utils::grid<char>& dst = ch.space;

    const int new_height = std::max(dst.height, offset.y + 1);
    dst.cells.resize(new_height * ch.width, '.');
    dst.width = ch.width;
    dst.height = new_height;

    for (size_t i = 0; i < src.cells.size(); i++) {
        if (src.cells[i] == ' ')
            continue;

        auto p = src.indexToPos(i) + offset;
        p.y = std::abs(p.y - (src.height - 1));
        auto elem = dst.lookup(p);
        assert(elem != nullptr);
        assert(*elem != '#');
        *elem = src.cells[i];
    }
}

bool move(const chamber& ch, piece& p, char mov) {
    const utils::grid<char>& shape = shapes[p.shape_index];

    int xinc = 0;
    switch (mov) {
        case '>': xinc = 1; break;
        case '<': xinc = -1; break;
    }

    if ((p.offset.x + xinc) < 0 || (p.offset.x + xinc + shape.width) > ch.width) {
        xinc = 0;
    } else {
        if (collision(ch.space, {.x = p.offset.x + xinc, .y = p.offset.y}, shape))
            xinc = 0;
    }
    p.offset.x += xinc;

    int yinc = -1;
    if (p.offset.y == (shape.height - 1) || collision(ch.space, {.x = p.offset.x, .y = p.offset.y + yinc}, shape))
        yinc = 0;
    p.offset.y += yinc;

    return yinc != 0;
}

void simulate(chamber& ch, movements& m, int n) {
    size_t shape_index = 0;
    while (n--) {
        const utils::grid<char>& shape = shapes[shape_index];
        piece p{
            .shape_index = shape_index,
            .offset = {.x = 2, .y = ch.space.height + shape.height + 2},
        };
        std::cout << "> " << p.offset << "\n";

        // prepare the shape for the next loop
        shape_index = (shape_index + 1) % 5;

        // move the piece through the camber
        while (move(ch, p, m.next()))
            std::cout << "> " << p.offset << "\n";

        paste(ch, p.offset, shape);
    }
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day17a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);

    movements moves;
    std::getline(in, moves.data);

    chamber ch;
    simulate(ch, moves, 11);

    std::cout << ch.space.height << "\n";
    std::cout << ch.space << "\n";
}
