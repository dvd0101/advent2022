#include <algorithm>
#include <bits/ranges_algo.h>
#include <fstream>
#include <iostream>
#include <stack>
#include <string_view>
#include <vector>

namespace ranges = std::ranges;

std::vector<char> parse_stacks_line(std::string_view line) {
    std::vector<char> stacks;

    size_t i = 0;
    while (i < line.size()) {
        stacks.push_back(line[i] == '[' ? line[i + 1] : ' ');
        i += 4;
    }
    return stacks;
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day5a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);

    std::vector<std::vector<char>> stacks1;
    std::vector<std::vector<char>> stacks2;

    std::string line;
    while (std::getline(in, line)) {
        if (line == "")
            break;

        auto s = parse_stacks_line(line);
        if (ranges::all_of(s, [](char ch) { return ch == ' '; })) {
            continue;
        }

        if (stacks1.empty())
            stacks1.resize(s.size());

        for (size_t i = 0; i < s.size(); i++)
            if (s[i] != ' ')
                stacks1[i].push_back(s[i]);
    }

    for (auto& s : stacks1)
        ranges::reverse(s);

    stacks2 = stacks1;

    std::string _;
    while (true) {
        int count, srcIx, dstIx;
        in >> _ >> count >> _ >> srcIx >> _ >> dstIx;
        if (!in)
            break;

        {
            auto& src = stacks1[srcIx - 1];
            auto& dst = stacks1[dstIx - 1];

            for (int n = 0; n < count; n++) {
                dst.push_back(src.back());
                src.pop_back();
            }
        }

        {
            auto& src = stacks2[srcIx - 1];
            auto& dst = stacks2[dstIx - 1];
            ranges::copy_n(src.end() - count, count, std::back_inserter(dst));
            src.resize(src.size() - count);
        }
    }

    for (auto& s : stacks1) {
        std::cout << (s.empty() ? ' ' : s.back());
    }
    std::cout << "\n";

    for (auto& s : stacks2) {
        std::cout << (s.empty() ? ' ' : s.back());
    }
    std::cout << "\n";
}
