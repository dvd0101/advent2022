#include <algorithm>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/container_hash/hash.hpp>
#include <fstream>
#include <iostream>
#include <ostream>
#include <ranges>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

struct node {
    std::string name;
    int rate;
    std::vector<std::string> links;
};

using graph = std::unordered_map<std::string, node>;

std::ostream& operator<<(std::ostream& os, const graph& g) {
    for (auto& [name, node] : g) {
        os << name << "/" << node.rate << " -> ";
        for (auto& l : node.links) {
            os << l << ", ";
        }
        os << "\n";
    }
    return os;
}

graph parse(std::istream&);

// The graph adjacency matrix calculated with the Floyd-Warshall algorithm;
// adj["AA"]["BB"] is the shortest distance between the two nodes
struct adj_matrix {
    int get_dist(const std::string& a, const std::string& b) const {
        auto i1 = data.find(a);
        if (i1 == data.end())
            return INT_MAX;

        auto i2 = i1->second.find(b);
        if (i2 == i1->second.end())
            return INT_MAX;

        return i2->second;
    }

    bool has_dist(const std::string& a, const std::string& b) const { return get_dist(a, b) < INT_MAX; }

    int set_dist(const std::string& a, const std::string& b, int value) {
        int old = get_dist(a, b);
        if (old <= value)
            return old;

        data[a][b] = value;
        return value;
    }
    std::unordered_map<std::string, std::unordered_map<std::string, int>> data;
};

adj_matrix floyd_warshall(const graph& g) {
    adj_matrix m;

    for (auto& [node_name, node] : g) {
        std::vector<std::pair<std::string, int>> stack = {{node_name, 0}};

        while (!stack.empty()) {
            auto [name, dist] = stack.back();
            stack.pop_back();

            m.set_dist(node_name, name, dist);

            for (auto& link : g.at(name).links) {
                if (m.get_dist(node_name, link) > dist + 1) {
                    stack.push_back({link, dist + 1});
                }
            }
        }
    }

    return m;
}

struct valves {
    void open(const std::string& v) {
        assert(!data.contains(v));
        data.insert(v);
    }

    bool contains(const std::string& v) const { return data.contains(v); }

    size_t hash() const {
        size_t seed = 0;
        for (auto& v : data)
            boost::hash_combine(seed, v);
        return seed;
    }

    std::set<std::string> data;
};

std::ostream& operator<<(std::ostream& os, const valves& v) {
    os << "[ ";
    for (auto& v : v.data)
        os << v << " ";
    os << "]";
    return os;
}

int visit(const graph& g, const adj_matrix& m) {
    struct visit_state {
        std::string node;
        int time;
        int pressure;
        valves open_valves;
    };

    // the results (we are searching for a global maximum, so this map will
    // contain all the local maximums)
    //
    // hash_of_opened_valves -> pressure
    std::unordered_map<size_t, int> results;

    visit_state initial{
        .node = "AA",
        .time = 30,
        .pressure = 0,
        .open_valves = {},
    };
    initial.open_valves.open("AA");
    std::vector<visit_state> stack = {initial};

    while (!stack.empty()) {
        auto state = std::move(stack.back());
        stack.pop_back();

        const size_t valves_hash = state.open_valves.hash();
        auto it = results.find(valves_hash);
        if (it == results.end() || it->second < state.pressure) {
            results.insert_or_assign(valves_hash, state.pressure);

            for (auto& [node_name, node] : g) {
                int dist = m.get_dist(state.node, node_name);
                // clang-format off
                if (node_name != state.node &&                        // it is not the same node
                    node.rate > 0 &&                                  // it has a non-zero flow rate  (otherwise it is meaningless to spend time to open it)
                    state.time > m.get_dist(state.node, node_name) && // the time needed to travel to the node is lesser than the time available (not equal because a need one unit of time to open the valve)
                    !state.open_valves.contains(node_name)) {         // the destination valve is still closed

                    auto v = state.open_valves;
                    v.open(node_name);
                    visit_state new_state{
                        .node = node_name,
                        .time = state.time - 1 - dist,
                        .pressure = state.pressure + (state.time - 1 - dist) * node.rate,
                        .open_valves = v,
                    };
                    stack.push_back(std::move(new_state));
                }
                // clang-format on
            }
        }
    }

    return *std::ranges::max_element(std::views::values(results));
}

int visit2(const graph& g, const adj_matrix& m) {
    struct visit_state {
        std::string node1;
        std::string node2;
        int time1;
        int time2;
        int pressure;
        valves open_valves;
    };

    // the results (we are searching for a global maximum, so this map will
    // contain all the local maximums)
    //
    // hash_of_opened_valves -> pressure
    std::unordered_map<size_t, int> results;

    visit_state initial{
        .node1 = "AA",
        .node2 = "AA",
        .time1 = 26,
        .time2 = 26,
        .pressure = 0,
        .open_valves = {},
    };
    initial.open_valves.open("AA");
    std::vector<visit_state> stack = {initial};

    int n = 0;
    while (!stack.empty()) {
        auto state = std::move(stack.back());
        stack.pop_back();
        n++;

        const size_t valves_hash = state.open_valves.hash();
        auto it = results.find(valves_hash);
        if (it == results.end() || it->second < state.pressure) {
            std::cout << n << " " << state.open_valves << " " << state.pressure << " <- "
                      << (it != results.end() ? it->second : 0) << "\n";
            std::cout << "    " << 26 - state.time1 << " / " << 26 - state.time2 << "\n";
            results.insert_or_assign(valves_hash, state.pressure);

            std::vector<std::string> closed_valves;
            for (const auto& [node_name, node] : g) {
                if (!state.open_valves.contains(node_name))
                    closed_valves.push_back(node_name);
            }

            std::ranges::sort(closed_valves, [&g](auto& a, auto& b) { return g.at(a).rate > g.at(b).rate; });

            for (const auto& node1_name : closed_valves) {
                /* auto node1_name = closed_valves[i]; */
                auto& node1 = g.at(node1_name);
                auto dist1 = m.get_dist(state.node1, node1_name);
                bool n1 = false;

                auto new_state = state;
                if (node1.rate > 0 && state.time1 > dist1 && !state.open_valves.contains(node1_name)) {
                    n1 = true;
                    auto v = state.open_valves;
                    v.open(node1_name);

                    new_state.node1 = node1_name;
                    new_state.time1 = state.time1 - 1 - dist1;
                    new_state.pressure = state.pressure + (state.time1 - 1 - dist1) * node1.rate;
                    new_state.open_valves = v;
                    stack.push_back(new_state);
                }

                for (const auto& node2_name : closed_valves) {
                    /* for (size_t j = 0; j < closed_valves.size(); j++) { */
                    /*     auto node2_name = closed_valves[j]; */
                    auto& node2 = g.at(node2_name);
                    auto dist2 = m.get_dist(state.node2, node2_name);
                    bool n2 = false;
                    /* if (node1_name == node2_name) */
                    /*     continue; */

                    if (node2.rate > 0 && state.time2 > dist2 && !new_state.open_valves.contains(node2_name)) {
                        n2 = true;
                    }

                    if (n2) {
                        auto s2 = new_state;
                        auto v = new_state.open_valves;
                        /* if (n1) { */
                        /*     v.open(node1_name); */
                        /*     new_state.node1 = node1_name; */
                        /*     new_state.time1 = state.time1 - 1 - dist1; */
                        /*     new_state.pressure = state.pressure + (state.time1 - 1 - dist1) * node1.rate; */
                        /* } */
                        /* if (n2) { */
                        v.open(node2_name);
                        s2.node2 = node2_name;
                        s2.time2 = state.time2 - 1 - dist2;
                        s2.pressure = state.pressure + (state.time2 - 1 - dist2) * node2.rate;
                        /* } */
                        s2.open_valves = v;
                        stack.push_back(s2);
                    }
                }
            }
        }
    }

    std::cout << n << "\n";
    return *std::ranges::max_element(std::views::values(results));
}

int visit3(const graph& g, const adj_matrix& m) {
    struct visit_state {
        std::string node;
        int time;
        int pressure;
        valves open_valves;
    };

    std::unordered_map<size_t, std::pair<int, valves>> results;

    visit_state initial{
        .node = "AA",
        .time = 26,
        .pressure = 0,
        .open_valves = {},
    };
    initial.open_valves.open("AA");
    std::vector<visit_state> stack = {initial};

    while (!stack.empty()) {
        auto state = std::move(stack.back());
        stack.pop_back();

        const size_t valves_hash = state.open_valves.hash();
        auto it = results.find(valves_hash);
        if (it == results.end() || it->second.first < state.pressure) {
            results.insert_or_assign(valves_hash, std::pair(state.pressure, state.open_valves));

            for (auto& [node_name, node] : g) {
                int dist = m.get_dist(state.node, node_name);
                // clang-format off
                if (node_name != state.node &&                        // it is not the same node
                    node.rate > 0 &&                                  // it has a non-zero flow rate  (otherwise it is meaningless to spend time to open it)
                    state.time > m.get_dist(state.node, node_name) && // the time needed to travel to the node is lesser than the time available (not equal because a need one unit of time to open the valve)
                    !state.open_valves.contains(node_name)) {         // the destination valve is still closed

                    auto v = state.open_valves;
                    v.open(node_name);
                    visit_state new_state{
                        .node = node_name,
                        .time = state.time - 1 - dist,
                        .pressure = state.pressure + (state.time - 1 - dist) * node.rate,
                        .open_valves = v,
                    };
                    stack.push_back(std::move(new_state));
                }
                // clang-format on
            }
        }
    }

    auto [pressure, open_valves] = *std::ranges::max_element( //
        std::views::values(results),                          //
        [](auto& a, auto& b) { return a.first < b.first; });  //

    std::cout << "X " << pressure << " " << open_valves << "\n";
    initial = visit_state{
        .node = "AA",
        .time = 26,
        .pressure = pressure,
        .open_valves = open_valves,
    };
    stack = {initial};

    while (!stack.empty()) {
        auto state = std::move(stack.back());
        stack.pop_back();

        const size_t valves_hash = state.open_valves.hash();
        auto it = results.find(valves_hash);
        std::cout << "loop\n";
        if (it == results.end() || it->second.first <= state.pressure) {
            results.insert_or_assign(valves_hash, std::pair(state.pressure, state.open_valves));

            for (auto& [node_name, node] : g) {
                std::cout << "try " << node_name << "\n";
                int dist = m.get_dist(state.node, node_name);
                // clang-format off
                if (node_name != state.node &&                        // it is not the same node
                    node.rate > 0 &&                                  // it has a non-zero flow rate  (otherwise it is meaningless to spend time to open it)
                    state.time > m.get_dist(state.node, node_name) && // the time needed to travel to the node is lesser than the time available (not equal because a need one unit of time to open the valve)
                    !state.open_valves.contains(node_name)) {         // the destination valve is still closed

                    auto v = state.open_valves;
                    v.open(node_name);
                    visit_state new_state{
                        .node = node_name,
                        .time = state.time - 1 - dist,
                        .pressure = state.pressure + (state.time - 1 - dist) * node.rate,
                        .open_valves = v,
                    };
                    stack.push_back(std::move(new_state));
                }
                // clang-format on
            }
        }
    }

    auto best = std::ranges::max_element(                    //
        std::views::values(results),                         //
        [](auto& a, auto& b) { return a.first < b.first; }); //

    return (*best).first;
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day16a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);
    auto g = parse(in);
    std::cout << g << "\n";

    auto adj_m = floyd_warshall(g);

    std::cout << visit(g, adj_m) << "\n";
    std::cout << visit2(g, adj_m) << "\n";
}

template <typename T>
void f(T&&);
graph parse(std::istream& in) {
    graph graph;
    std::string l;
    while (true) {
        node n;

        in.ignore(6);
        in >> n.name;
        if (!in)
            break;

        in.ignore(100, '=');
        in >> n.rate;

        std::getline(in, l);

        std::vector<std::string> splits;
        boost::split(splits, l, boost::is_any_of(" ,"));
        for (auto& s : std::views::reverse(splits)) {
            if (s.size() == 2)
                n.links.push_back(s);
            else if (s.empty())
                continue;
            else
                break;
        }

        graph[n.name] = std::move(n);
    }

    return graph;
}
