% : %.cpp *.h
	g++ -std=c++20 -g -Wall -Wextra -fsanitize=undefined -fsanitize=address -fconcepts-diagnostics-depth=2 $< -o $@
