#include <algorithm>
#include <cassert>
#include <compare>
#include <fstream>
#include <iostream>
#include <memory>
#include <ostream>
#include <stack>
#include <string>
#include <vector>

struct elem {
    int value = 0;
    std::unique_ptr<std::vector<elem>> sub = nullptr;

    static elem make_num(int value) { return {.value = value, .sub = nullptr}; }

    static elem make_list() { return {.sub = std::make_unique<std::vector<elem>>()}; }
    static elem make_list(int value) {
        elem t = make_list();
        t.sub->push_back({.value = value});
        return t;
    }

    bool is_number() const { return !sub; }

    friend bool operator==(const elem& a, const elem& b) { return a <=> b == 0; }
    friend std::strong_ordering operator<=>(const elem& a, const elem& b) {
        auto equal = std::strong_ordering::equal;
        auto less = std::strong_ordering::less;
        auto greater = std::strong_ordering::greater;

        if (a.is_number() && b.is_number()) {
            if (a.value == b.value)
                return equal;
            return a.value < b.value ? less : greater;
        } else if (!a.is_number() && !b.is_number()) {
            auto& list1 = *a.sub;
            auto& list2 = *b.sub;

            for (size_t i = 0; i < std::min(list1.size(), list2.size()); i++) {
                if (list1[i] < list2[i])
                    return less;
                else if (list1[i] > list2[i])
                    return greater;
            }

            if (list1.size() == list2.size())
                return equal;
            return list1.size() < list2.size() ? less : greater;
        } else if (a.is_number() && !b.is_number()) {
            return elem::make_list(a.value) <=> b;
        } else {
            return a <=> elem::make_list(b.value);
        }
    }
};
using packet = std::vector<elem>;
using packet_pair = std::pair<packet, packet>;

std::vector<packet_pair> parse(std::istream& in);

std::ostream& operator<<(std::ostream& os, const packet& p) {
    os << '[';
    for (auto& e : p) {
        if (e.is_number())
            os << e.value << ", ";
        else
            os << *e.sub << ", ";
    }
    os << ']';
    return os;
};

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day13a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);
    auto pairs = parse(in);

    int sum = 0;
    for (size_t i = 0; i < pairs.size(); i++) {
        auto& [p1, p2] = pairs[i];
        if (p1 < p2)
            sum += i + 1;
    }

    std::cout << sum << "\n";

    auto make_divider = [](int value) {
        packet d;
        d.push_back(elem::make_list(value));
        return d;
    };

    const packet divider1 = make_divider(2);
    const packet divider2 = make_divider(6);

    std::vector<packet> l;
    l.push_back(make_divider(2));
    l.push_back(make_divider(6));
    for (auto& [p1, p2] : pairs) {
        l.push_back(std::move(p1));
        l.push_back(std::move(p2));
    }

    std::ranges::sort(l);

    int key = 1;
    for (size_t i = 0; i < l.size(); i++) {
        if (l[i] == divider1 || l[i] == divider2) {
            key *= i + 1;
        }
    }
    std::cout << key << "\n";
}

std::vector<packet_pair> parse(std::istream& in) {
    auto parse_packet = [](std::istream& in) {
        packet p;

        std::string line;
        if (!std::getline(in, line))
            return p;

        std::stack<packet*> stack;
        stack.push(&p);

        for (size_t i = 1; i < line.size() - 1; i++) {
            char ch = line[i];
            if (ch == '[') {
                packet* curr = stack.top();
                if (curr->empty()) {
                    curr->push_back({});
                }
                assert(curr->back().value == 0);
                curr->back().sub = std::make_unique<packet>();
                stack.push(curr->back().sub.get());
            } else if (ch == ']') {
                stack.pop();
            } else if (ch >= '0' && ch <= '9') {
                packet* curr = stack.top();
                if (curr->empty()) {
                    curr->push_back({});
                }
                assert(!curr->back().sub);
                curr->back().value = curr->back().value * 10 + ch - '0';
            } else if (ch == ',') {
                packet* curr = stack.top();
                curr->push_back({});
            }
        }

        return p;
    };

    std::vector<packet_pair> out;
    while (true) {
        packet p1 = parse_packet(in);
        if (!in)
            break;

        packet p2 = parse_packet(in);
        if (!in)
            break;

        out.push_back({std::move(p1), std::move(p2)});

        in.ignore(1);
    }

    return out;
}
