#include <climits>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <optional>
#include <ostream>
#include <unordered_set>

#include "./geo.h"
#include "./range.h"

using pos = utils::pos;
struct sensor_data {
    pos beacon;
    int dist;
};

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day15a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);

    std::unordered_map<pos, sensor_data> sensors;

    while (true) {
        int x, y;
        in.ignore(1000, '=');
        in >> x;
        in.ignore(1000, '=');
        in >> y;
        if (!in)
            break;

        pos s{x, y};

        in.ignore(1000, '=');
        in >> x;
        in.ignore(1000, '=');
        in >> y;

        int dist = std::abs(x - s.x) + std::abs(y - s.y);
        sensors.insert({s, {.beacon = {x, y}, .dist = dist}});
    }

    const int row = 2000000;
    /* const int row = 10; */
    int x1 = INT_MAX;
    int x2 = INT_MIN;
    for (auto& [sensor, data] : sensors) {
        if (std::abs(row - sensor.y) <= data.dist) {
            int dy = std::abs(sensor.y - row);
            x1 = std::min(x1, sensor.x - (data.dist - dy));
            x2 = std::max(x2, sensor.x + (data.dist - dy));
        }
    }
    std::cout << x2 - x1 << "\n";

    const int limit = 4'000'000;
    std::unordered_map<int, std::vector<utils::range>> uncovered;
    auto add_range = [&](int row, utils::range r) {
        std::vector<utils::range> new_ranges;
        for (auto& old : uncovered[row]) {
            auto diff = old - r;
            if (diff.n == 0)
                continue;
            else if (diff.n == 1)
                new_ranges.push_back(diff.a);
            else {
                new_ranges.push_back(diff.a);
                new_ranges.push_back(diff.b);
            }
        }

        uncovered[row] = std::move(new_ranges);
    };

    for (auto& [sensor, data] : sensors) {
        for (int y = sensor.y - data.dist, i = 0; y <= sensor.y + data.dist; y++) {
            if (y >= 0 && y <= limit) {
                uncovered.insert({y, std::vector{utils::range{.start = 0, .end = limit}}});

                utils::range r = {.start = std::max(sensor.x - i, 0), .end = std::min(sensor.x + i, limit)};
                add_range(y, r);
            }
            if (y < sensor.y)
                i++;
            else
                i--;
        }
    }

    for (auto& [y, rs] : uncovered) {
        if (rs.empty())
            continue;
        assert(rs.size() == 1);
        assert(rs[0].start == rs[0].end);
        std::cout << y + (int64_t)rs[0].start * 4000000 << "\n";
    }
}
