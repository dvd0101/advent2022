#include <algorithm>
#include <cstdio>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

struct player {
    int id;
    std::vector<int64_t> items;
    std::function<int64_t(int64_t)> operation;
    int test;
    int target_true;
    int target_false;

    int inspected_items = 0;
};

std::vector<player> parse(std::istream&);

void make_round(std::vector<player>& monkeys, bool reduce_item) {
    int mod = 1;
    for (auto& p : monkeys)
        mod *= p.test;

    for (auto& p : monkeys) {
        for (auto item : p.items) {
            item = p.operation(item);
            if (reduce_item)
                item /= 3;
            int target = item % p.test == 0 ? p.target_true : p.target_false;
            monkeys[target].items.push_back(item % mod);
        }
        p.inspected_items += p.items.size();
        p.items.clear();
    }
}

std::ostream& operator<<(std::ostream& dest, __int128_t value) {
    std::ostream::sentry s(dest);
    if (s) {
        __uint128_t tmp = value < 0 ? -value : value;
        char buffer[128];
        char* d = std::end(buffer);
        do {
            --d;
            *d = "0123456789"[tmp % 10];
            tmp /= 10;
        } while (tmp != 0);
        if (value < 0) {
            --d;
            *d = '-';
        }
        int len = std::end(buffer) - d;
        if (dest.rdbuf()->sputn(d, len) != len) {
            dest.setstate(std::ios_base::badbit);
        }
    }
    return dest;
}

void print_best(const std::vector<player>& monkeys) {
    std::vector<__int128> best;
    for (auto& p : monkeys) {
        best.push_back(p.inspected_items);
    }

    std::ranges::sort(best);
    __int128_t x = best[best.size() - 1] * best[best.size() - 2];
    std::cout << x << "\n";
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day11a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);
    auto monkeys = parse(in);

    {
        auto m = monkeys;
        for (int i = 0; i < 20; i++) {
            make_round(m, true);
        }

        print_best(m);
    }

    {
        auto m = monkeys;
        for (int i = 0; i < 10000; i++) {
            make_round(m, false);
        }

        print_best(m);
    }
}

std::vector<player> parse(std::istream& in) {
    std::vector<player> monkeys;

    std::string l;
    while (true) {
        player p;
        in >> l >> p.id >> l;
        if (!in)
            break;

        std::getline(in, l, ':');
        while (true) {
            char ch;
            int64_t x;
            in >> x;
            if (!in)
                throw std::runtime_error("item not found");
            p.items.push_back(x);

            in.get(ch);
            if (ch == '\n')
                break;
        }

        std::getline(in, l, '=');
        {
            char op;
            int coeff;
            bool variable = false;

            in.ignore(4);
            in >> op;
            in >> coeff;
            if (!in) {
                in.clear();
                in >> l;
                if (l != "old")
                    throw std::runtime_error("invalid operation");
                variable = true;
            }

            p.operation = [op, coeff, variable](int64_t v) {
                switch (op) {
                    case '+': v = v + (variable ? v : coeff); break;
                    case '-': v = v - (variable ? v : coeff); break;
                    case '/': v = v / (variable ? v : coeff); break;
                    case '*': v = v * (variable ? v : coeff); break;
                };
                return v;
            };
        }

        std::getline(in, l, 'y');
        in >> p.test;

        std::getline(in, l, 'y');
        in >> p.target_true;

        std::getline(in, l, 'y');
        in >> p.target_false;

        if (!in)
            break;
        monkeys.push_back(std::move(p));
    }

    return monkeys;
}
