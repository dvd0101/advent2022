#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>
#include <string>
#include <string_view>
#include <vector>

namespace ranges = std::ranges;

int priority(char c) {
    if (c >= 'a' && c <= 'z')
        return c - 'a' + 1;
    return c - 'A' + 27;
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day3a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);

    int priorities = 0;
    int badge_priorities = 0;

    std::string line;
    std::vector<std::set<char>> group;
    while (std::getline(in, line)) {
        std::set comp1(line.data(), line.data() + line.size() / 2);
        std::set comp2(line.data() + line.size() / 2, line.data() + line.size());

        group.emplace_back(line.begin(), line.end());

        line.clear();
        ranges::set_intersection(comp1, comp2, std::back_inserter(line));
        priorities += priority(line[0]);

        if (group.size() == 3) {
            line.clear();
            ranges::set_intersection(group[0], group[1], std::back_inserter(line));

            std::set r(line.begin(), line.end());
            line.clear();
            ranges::set_intersection(r, group[2], std::back_inserter(line));

            badge_priorities += priority(line[0]);

            group.clear();
        }
    }

    std::cout << priorities << "\n";
    std::cout << badge_priorities << "\n";
}
