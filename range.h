#pragma once
#include <boost/container_hash/hash.hpp>
#include <optional>
#include <ostream>

namespace utils {
    struct range {
        int start = 0;
        int end = 0;

        auto operator<=>(const range&) const = default;
    };

    inline std::optional<range> operator&(range a, range b) {
        if (b.start > a.end)
            return {};
        if (b.end < a.start)
            return {};

        return range{
            .start = a.start < b.start ? b.start : a.start,
            .end = a.end > b.end ? b.end : a.end,
        };
    }

    inline std::ostream& operator<<(std::ostream& os, range a) { return os << '[' << a.start << " - " << a.end << ']'; }

    struct diff_result {
        int n = 0;
        range a = {};
        range b = {};
    };
    /**
     *  1.
     *  a  -------
     *  b      -------
     *  -> ----
     *
     *  2.
     *  a       ------
     *  b   -----
     *  ->       -----
     *
     *  3.
     *  a  ----
     *  b        ----
     *  -> ----
     *
     *  4.
     *  a     ---
     *  b  ---------
     *  -> (empty)
     *
     *  5.
     *  a   ---------
     *  b       ---
     *  ->  ----   ----
     */
    inline diff_result operator-(range a, range b) {
        auto i = a & b;
        if (!i) {
            // 3.
            return {.n = 1, .a = a};
        }

        range c = *i;
        if (c == a) {
            // 4.
            return {.n = 0};
        }

        if (c.start > a.start && c.end >= a.end) {
            // 1
            return {.n = 1, .a = {.start = a.start, .end = c.start - 1}};
        }

        if (c.end < a.end && c.start <= a.start) {
            // 2
            return {.n = 1, .a = {.start = c.end + 1, .end = a.end}};
        }

        return {
            .n = 2,
            .a = {.start = a.start, .end = c.start - 1},
            .b = {.start = c.end + 1, .end = a.end},
        };
    }

}

template <>
struct std::hash<utils::range> {
    std::size_t operator()(utils::range const& r) const noexcept {
        std::size_t seed = 0;

        boost::hash_combine(seed, r.start);
        boost::hash_combine(seed, r.end);

        return seed;
    }
};
