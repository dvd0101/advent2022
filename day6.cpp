#include <array>
#include <fstream>
#include <iostream>

template <size_t n>
bool start_of_marker(std::array<char, n> marker, size_t start, size_t len) {
    for (size_t ix = 0; ix < len; ix++) {
        for (size_t iy = ix + 1; iy < len; iy++) {
            if (marker[(ix + start) % n] == marker[(iy + start) % n])
                return false;
        }
    }
    return true;
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day6a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);

    constexpr size_t n = 14;
    std::array<char, n> buffer;
    int start_pos = -1;
    int message_pos = -1;
    {
        int pos = 0;
        int rx1 = 0;
        int rx2 = 0;
        int wx = 0;
        while (true) {
            char ch;

            in >> ch;
            if (!in)
                break;

            pos++;
            buffer[wx % n] = ch;
            wx++;
            if (start_pos == -1 && pos >= 4) {
                if (start_of_marker(buffer, rx1, 4)) {
                    start_pos = pos;
                }
                rx1++;
            }

            if (message_pos == -1 && pos >= 14) {
                if (start_of_marker(buffer, rx2, 14)) {
                    message_pos = pos;
                }
                rx2++;
            }

            if (start_pos != -1 && message_pos != -1)
                break;
        }
    }
    std::cout << start_pos << "\n";
    std::cout << message_pos << "\n";
}
