#pragma once
#include <boost/heap/fibonacci_heap.hpp>
#include <functional>
#include <unordered_map>

#include "./geo.h"

namespace utils {
    template <typename T>
    std::tuple<int64_t, std::vector<size_t>> dijkstra(const grid<T>& g,
                                                      size_t start,
                                                      size_t stop,
                                                      std::function<int64_t(size_t, size_t)> distFn) {
        struct queue_item {
            size_t index;
            int64_t dist;

            bool operator>(const queue_item& b) const { return dist > b.dist; }
        };

        using boost::heap::fibonacci_heap;
        using priority_queue = fibonacci_heap<queue_item, boost::heap::compare<std::greater<queue_item>>>;
        priority_queue queue;

        struct dist_item {
            int64_t dist;
            typename priority_queue::handle_type queue_handle;
        };

        std::vector<dist_item> dists;
        for (size_t i = 0; i < g.cells.size(); i++) {
            int64_t d = i == start ? 0 : INT64_MAX;
            auto handle = queue.push({.index = i, .dist = d});
            dists.push_back({d, handle});
        }

        std::unordered_map<size_t, size_t> prev;

        while (!queue.empty()) {
            auto [curr, curr_dist] = queue.top();
            queue.pop();

            if (curr_dist == INT64_MAX) {
                // unreachable
                return {-1, {}};
            }

            if (curr == stop)
                break;

            for (size_t n : g.neighbor(curr)) {
                auto nd = distFn(curr, n);
                if (nd == INT64_MAX)
                    continue;

                auto new_d = curr_dist + nd;
                if (new_d < dists[n].dist) {
                    dists[n].dist = new_d;
                    prev[n] = curr;
                    queue.update(dists[n].queue_handle, {.index = n, .dist = new_d});
                }
            }
        }

        std::vector<size_t> path;
        for (size_t x = stop; prev.contains(x); x = prev[x]) {
            path.push_back(x);
        }
        path.push_back(start);
        std::ranges::reverse(path);

        return {dists[stop].dist, path};
    }
}
