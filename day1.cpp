#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <vector>

namespace ranges = std::ranges;

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day1a";
    if (argc > 1)
        input_name = argv[1];

    std::vector<int> elves = {0};

    std::ifstream in(input_name);

    std::string line;
    while (std::getline(in, line)) {
        if (line == "") {
            elves.push_back(0);
        } else {
            elves.back() += std::atoi(line.data());
        }
    }

    ranges::sort(elves, std::greater<>{});
    std::cout << elves[0] << "\n";
    std::cout << elves[0] + elves[1] + elves[2] << "\n";
}
