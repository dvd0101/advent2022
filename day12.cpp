#include <fstream>
#include <iostream>
#include <string>

#include "./dijkstra.h"

struct map {
    utils::grid<char> grid;
    int start;
    int target;
};

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day12a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);

    map m;
    int start = 0;
    int target = 0;
    while (true) {
        char ch;
        in >> ch;
        if (!in)
            break;

        if (ch == 'S') {
            start = m.grid.cells.size();
            ch = 'a';
        } else if (ch == 'E') {
            target = m.grid.cells.size();
            ch = 'z';
        }

        m.grid.cells.push_back(ch);
        if (m.grid.width == 0 && in.peek() == '\n')
            m.grid.width = m.grid.cells.size();
    }

    m.grid.height = m.grid.cells.size() / m.grid.width;
    m.start = start;
    m.target = target;

    auto weight = [&](size_t a, size_t b) {
        int64_t d = m.grid.cells[b] - m.grid.cells[a];
        return d > 1 ? INT64_MAX : 1;
    };

    auto shortest_path = dijkstra(m.grid, m.start, m.target, weight);

    auto& path = std::get<1>(shortest_path);
    std::cout << path.size() - 1 << "\n";

    size_t min = INT_MAX;
    for (size_t i = 0; i < m.grid.cells.size(); i++) {
        if (m.grid.cells[i] != 'a') {
            continue;
        }
        auto shortest_path = dijkstra(m.grid, i, m.target, weight);
        auto& path = std::get<1>(shortest_path);
        if (path.size())
            min = std::min(min, path.size() - 1);
    }
    std::cout << min << "\n";
}
