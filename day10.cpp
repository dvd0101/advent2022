#include <fstream>
#include <iostream>
#include <vector>

#include "./generator.h"

struct ins {
    char op;
    int arg;
};

struct cpu {
    int reg_x = 1;
};

utils::generator<int> execute(cpu& cpu, const std::vector<ins>& prg) {
    int cycle = 1;

    for (const auto [op, arg] : prg) {
        co_yield cycle;
        switch (op) {
            case 'n': break;
            case 'a':
                co_yield ++cycle;
                cpu.reg_x += arg;
                break;
        };
        cycle++;
    }
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day10a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);
    std::vector<ins> prg;
    while (true) {
        ins i;
        in >> i.op;
        if (!in)
            break;

        in.ignore(3);
        if (i.op == 'a')
            in >> i.arg;
        prg.push_back(i);
    }

    cpu cpu;
    int crt = 0;
    int signal_strength = 0;
    for (auto cycle : execute(cpu, prg)) {
        if ((cycle + 20) % 40 == 0) {
            signal_strength += cycle * cpu.reg_x;
        }

        char ch = '.';
        if (crt >= cpu.reg_x - 1 && crt <= cpu.reg_x + 1)
            ch = '#';

        std::cout << ch;

        crt++;
        if (crt == 40) {
            crt = 0;
            std::cout << "\n";
        }
    }
    std::cout << signal_strength << "\n";
}
