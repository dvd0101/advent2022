#include <charconv>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "./generator.h"

struct file {
    int64_t size;
    std::string name;
};

struct dir {
    std::string name;
    dir* parent;

    std::vector<file> files;
    std::vector<dir> subdirs;

    dir(std::string n, dir* p = nullptr) : name{std::move(n)}, parent{p} {}

    int64_t size() const {
        int64_t s = 0;
        for (auto& f : files)
            s += f.size;
        for (auto& subdir : subdirs)
            s += subdir.size();
        return s;
    }
};

dir parse(std::istream&);
void print(const dir&, std::ostream&);

utils::generator<const dir*> walk(const dir& root) {
    co_yield &root;

    using elem = std::pair<const std::vector<dir>*, size_t>;
    std::vector<elem> stack = {{&root.subdirs, 0}};

    while (!stack.empty()) {
        auto [subdirs, index] = stack.back();
        if (index == subdirs->size()) {
            // branch explored
            stack.pop_back();
        } else {
            const dir* subdir = &(*subdirs)[index];
            stack.back().second++;
            stack.push_back({&subdir->subdirs, 0});
            co_yield subdir;
        }
    }
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day7a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);
    dir root = parse(in);

    int64_t sub_100k_size = 0;
    for (const dir* ptr : walk(root)) {
        auto n = ptr->size();
        if (n < 100'000)
            sub_100k_size += n;
    }
    std::cout << sub_100k_size << "\n";

    const int64_t disk_size = 70'000'000;
    const int64_t space_required = 30'000'000;
    const int64_t space_needed = space_required - (disk_size - root.size());

    std::pair<const dir*, int64_t> to_delete = {&root, root.size()};
    for (const dir* ptr : walk(root)) {
        auto n = ptr->size();
        if (n >= space_needed && n < to_delete.second)
            to_delete = {ptr, n};
    }

    std::cout << to_delete.second << "\n";
}

dir parse(std::istream& in) {
    dir root("/");
    dir* cwd = &root;

    std::string line;
    bool list_mode = false;
    while (std::getline(in, line)) {
        if (line.empty())
            continue;

        if (line.starts_with("$ cd")) {
            list_mode = false;
            std::string dst = line.substr(5);

            if (dst == "/") {
                cwd = &root;
            } else if (dst == "..") {
                cwd = cwd->parent;
            } else {
                dir* target = nullptr;
                for (auto& d : cwd->subdirs) {
                    if (d.name == dst) {
                        target = &d;
                    }
                }

                if (target == nullptr)
                    throw std::runtime_error("subdir not found");
                cwd = target;
            }

        } else if (line.starts_with("$ ls")) {
            list_mode = true;
        } else if (list_mode) {
            if (line.starts_with("dir")) {
                dir subdir(line.substr(4), cwd);
                cwd->subdirs.push_back(std::move(subdir));
            } else {
                int size = 0;
                auto r = std::from_chars(&line[0], &line[0] + line.size(), size);
                if (r.ec != std::errc()) {
                    std::runtime_error("size not found");
                }
                cwd->files.push_back({size, r.ptr + 1});
            }
        } else {
            throw std::runtime_error("unknown command");
        }
    }

    return root;
}

void print(const dir& d, std::ostream& os, std::string indent) {
    os << indent << "- " << d.name << " (dir)\n";
    indent += "  ";

    for (auto& subdir : d.subdirs) {
        print(subdir, os, indent);
    }

    for (auto& f : d.files) {
        os << indent << "- " << f.name << " (file, size=" << f.size << ")\n";
    }
}

void print(const dir& d, std::ostream& os) { print(d, os, ""); }
