#include <fstream>
#include <iostream>
#include <set>
#include <vector>

#include "./generator.h"

struct command {
    char d;
    int count;
};

struct pos {
    int x = 0;
    int y = 0;

    bool operator==(const pos&) const = default;
    auto operator<=>(const pos&) const = default;
};

std::ostream& operator<<(std::ostream& os, pos pt) { return os << "(" << pt.x << "," << pt.y << ")"; }

struct dir {
    int dx = 0;
    int dy = 0;
};

struct knot {
    pos pt;

    knot operator+(dir d) {
        knot a = *this;
        a.pt.x += d.dx;
        a.pt.y += d.dy;
        return a;
    }
};

template <size_t N>
struct rope {
    std::array<knot, N> nodes;

    const knot& tail() const { return nodes.back(); }

    void move(dir d) {
        knot& head = nodes.front();

        head = head + d;

        for (size_t ix = 1; ix < nodes.size(); ix++) {
            knot& lead = nodes[ix - 1];
            knot& curr = nodes[ix];

            const int lx = lead.pt.x - curr.pt.x;
            const int ly = lead.pt.y - curr.pt.y;
            const bool adjacent = std::abs(lx) <= 1 && std::abs(ly) <= 1;

            if (!adjacent) {
                dir d;
                if (lx != 0)
                    d.dx = lx / std::abs(lx);
                if (ly != 0)
                    d.dy = ly / std::abs(ly);
                curr = curr + d;
            }
        }
    }
};

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day9a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);
    std::vector<command> cmds;
    while (true) {
        command cmd;
        in >> cmd.d >> cmd.count;
        if (!in)
            break;
        cmds.push_back(cmd);
    }

    rope<2> r1;
    rope<10> r2;
    std::set<pos> pos1;
    std::set<pos> pos2;

    pos z{-1, -1};
    for (auto cmd : cmds) {
        dir d;
        switch (cmd.d) {
            case 'R': d.dx = 1; break;
            case 'U': d.dy = 1; break;
            case 'L': d.dx = -1; break;
            case 'D': d.dy = -1; break;
        }
        while (cmd.count-- > 0) {
            r1.move(d);
            r2.move(d);
            pos1.insert(r1.tail().pt);
            pos2.insert(r2.tail().pt);
            if (r2.tail().pt != z) {
                std::cout << r2.tail().pt << "\n";
                z = r2.tail().pt;
            }
        }
    }

    std::cout << pos1.size() << "\n";
    std::cout << pos2.size() << "\n";
}
