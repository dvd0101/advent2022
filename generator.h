#pragma once
#include <concepts>
#include <coroutine>
#include <optional>
#include <utility>

#include <ranges>

namespace utils {
    template <typename T>
    class generator {
      public:
        struct promise_type;
        using handle_type = std::coroutine_handle<promise_type>;

        struct promise_type {
            generator<T> get_return_object() { return generator(handle_type::from_promise(*this)); };

            std::suspend_always initial_suspend() { return {}; }
            std::suspend_always final_suspend() noexcept { return {}; }
            void unhandled_exception() { exception_ = std::current_exception(); }
            void return_void() {}

            template <std::convertible_to<T> From>
            std::suspend_always yield_value(From&& from) {
                value_ = std::forward<From>(from);
                return {};
            }

            void rethrow_if_exception() {
                if (exception_)
                    std::rethrow_exception(exception_);
            }

            T value_;
            std::exception_ptr exception_;
        };

      private:
        generator(handle_type h) : handle_(h) {}

      public:
        generator() noexcept = default;
        ~generator() {
            if (handle_) {
                handle_.destroy();
            }
        }

        generator(const generator&) = delete;
        generator(generator&& rhs) noexcept : handle_(std::exchange(rhs.handle_, nullptr)) {}
        generator& operator=(const generator&) = delete;
        generator& operator=(generator&& rhs) noexcept {
            if (handle_) {
                handle_.destroy();
                handle_ = nullptr;
            }
            handle_ = std::exchange(rhs.handle_, nullptr);
            return *this;
        }
        void swap(generator& other) noexcept { std::swap(handle_, other.handle_); }

      public:
        class sentinel {};

        class iterator {
          public:
            using value_type = std::remove_reference_t<T>;
            using reference_type = value_type&;
            using pointer_type = value_type*;
            using difference_type = std::ptrdiff_t;

          private:
            friend class generator;
            iterator(handle_type handle) : handle_(handle) {}

          public:
            iterator() = default;
            ~iterator() {
                if (handle_) {
                    handle_.destroy();
                }
            }

            // Non-copyable because coroutine handles point to a unique resource
            iterator(const iterator&) = delete;
            iterator(iterator&& rhs) noexcept : handle_(std::exchange(rhs.handle_, nullptr)) {}
            iterator& operator=(const iterator&) = delete;
            iterator& operator=(iterator&& rhs) noexcept {
                if (handle_) {
                    handle_.destroy();
                    handle_ = nullptr;
                }
                handle_ = std::exchange(rhs.handle_, nullptr);
                return *this;
            }

            friend bool operator==(const iterator& it, sentinel) noexcept { return (!it.handle_ || it.handle_.done()); }

            iterator& operator++() {
                handle_.resume();
                if (handle_.done()) {
                    handle_.promise().rethrow_if_exception();
                }
                return *this;
            }

            void operator++(int) { (void)this->operator++(); }

            reference_type operator*() const noexcept(noexcept(std::is_nothrow_copy_constructible_v<reference_type>)) {
                return handle_.promise().value_;
            }

          private:
            handle_type handle_;
        };

        iterator begin() {
            handle_.resume();
            if (handle_.done()) {
                handle_.promise().rethrow_if_exception();
            }
            return {std::exchange(handle_, nullptr)};
        }

        sentinel end() const noexcept { return {}; }

      private:
        handle_type handle_;
    };
}

template <class T>
inline constexpr bool std::ranges::enable_view<utils::generator<T>> = true;
