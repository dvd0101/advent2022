default rel

global main

extern printf
extern foobar


section .note.GNU-stack noalloc noexec nowrite progbits

; abi arguments    : rdi rsi rdx rcx r8 r9
; abi callee saved : rbx rsp rbp r12 r13 r14 r15 fs
; syscall arguments: rdi rsi rdx r10 r8 r9

; /usr/include/x86_64-linux-gnu/asm/unistd_64.h
SYS_WRITE   equ 1
SYS_OPEN    equ 2
SYS_LSEEK   equ 8
SYS_MMAP    equ 9

STDOUT      equ 1

SEEK_START  equ 0
SEEK_CUR    equ 1
SEEK_END    equ 2

; /usr/include/x86_64-linux-gnu/bits/mman-linux.h
PROT_READ   equ 1
MAP_PRIVATE equ 2

;
; Initialised data goes here
;
SECTION .data
log_fmt         db `%s\n`, 0
log_fmt_int     db `%s: %d\n`, 0
log_fmt_ptr     db `%s: [%p]\n`, 0

input_file      db "./inputs/day1a", 0
open_file_msg   db "file descriptor", 0
file_size_msg   db "input size", 0
mmap_addr_msg   db "mmap address", 0
trace_msg       db "TRACE", 0
input_err_msg   db "cannot open the input file:", 0
mmap_err_msg    db "cannot mmap the input file:", 0

hello           db "Hello World!", 0   ; const char *
hello_len       equ $ - hello               ; size_t
input_fd        dd 0
input_size      dq 0
data_ptr        dq 0

;
; Code goes here
;
SECTION .text

foobar2:
    mov eax, 20
    mul edi
    ret

foobar3:
    lea eax, [rdi * 5]
    add eax, eax
    ret

foobar4:
    lea eax, [rdi + 2]
    add eax, eax
    ret

; logs a message using the `printf` function
; arguments:
;   - rsi   address to the, null-terminated, message
log_msg:
    xor     rax, rax
    lea     rdi, [log_fmt]
    call    printf wrt ..plt
    ret

; logs a message + int using the `printf` function
; arguments:
;   - rsi   address to the, null-terminated, message
;   - rdx   int to print
log_msg_int:
    xor     rax, rax
    lea     rdi, [log_fmt_int]
    call    printf wrt ..plt
    ret

; logs a message + ptr
log_msg_ptr:
    xor     rax, rax
    lea     rdi, [log_fmt_ptr]
    call    printf wrt ..plt
    ret

print2:
    mov     rax, SYS_WRITE
    mov     rdi, STDOUT
    syscall
    ret

; opena file by calling the `open` syscall
; arguments:
;   - rdi   address to the, null-terminated, filename
;   - rsi   flags
;
; return value:
;   - rax   the file descriptor
open_file:
    mov     rax, SYS_OPEN
    syscall
    ret

; returns the file size by calling the `lseek` syscall
;
; arguments:
;   - rdi   the file descriptor
;
; return value:
;   - rax   the file size
;
; note: the file current position is preserved and in order to do so the
; `lseek` syscall is called three times
file_size:
    ; the current position (as bytes from the start)
    mov     rax, SYS_LSEEK
    mov     rsi, 0
    mov     rdx, SEEK_CUR
    syscall
    mov     r11, rax

    ; the file size
    mov     rax, SYS_LSEEK
    mov     rsi, 0
    mov     rdx, SEEK_END
    syscall
    mov     r10, rax

    ; restore the previous position
    mov     rax, SYS_LSEEK
    mov     rsi, r11
    mov     rdx, SEEK_START
    syscall
    
    mov     rax, r10
    ret

; maps a file descriptor by calling the `mmap` syscall
;
; arguments:
;   - rdi   the file descriptor to map
;   - rsi   the length in bytes of the region to map
;
; return value:
;   - rax   a pointer to the map area or -1 in case of error
mmap_fd:
    ; void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
    ;            rdi,        rsi,           rdx,      r10,       r8,     r9
    mov     r8, rdi             ; fd
    mov     rdi, 0              ; addr
    mov     rdx, PROT_READ      ; prot
    mov     r10, MAP_PRIVATE    ; flags
    mov     r9, 0               ; offset
    mov     rax, SYS_MMAP
    syscall
    ret

; convert a null-terminated string to an integer.
;
; arguments:
;   - rdi   pointer to a null-terminated string
;
; return value:
;   - rax   the number corrisponding to the content of str
;   - rcx   the number of character converted
;
; conversion stops at the first non numeric character
atoi:
    push    rbx

    xor     rax, rax
    xor     rbx, rbx
    xor     r8, r8
    xor     rdx, rdx

    mov     ecx, 10

.loop:
    lea     rsi, [rdi + r8]

    mov     bl, [rsi]

    ; bl >= '0' &&  bl <= '9'
    cmp     bl, '0'
    jl      atoi.exit
    cmp     bl, '9'
    jg      atoi.exit

    mul     ecx
    sal     rdx, 32
    or      rax, rdx

    sub     bl, '0'
    add     rax, rbx

    inc     r8
    jmp     atoi.loop
.exit:
    mov     rcx, r8
    pop     rbx
    ret

; count the number of whitespace chars.
;
; arguments:
;   - rdi   pointer to a null-terminated string
;
; return value:
;   - rax   the number of whitespace chars
;
; the counter stops at the first non-whitespace (not ' ' nor '\n')
count_ws:
    xor     rax, rax
    xor     rdx, rdx

.loop:
    mov     dl, [rdi + rax]

    push rdx
    lea     rsi, [trace_msg]
    call    log_msg_int
    pop rdx

    cmp     dl, 0
    je      count_ws.exit

    ; dl == ' ' || dl == '\n'
    cmp     dl, `\n`
    je      count_ws.continue
    cmp     dl, ' '
    je      count_ws.continue
    jmp     count_ws.exit

.continue:
    inc     rax
    jmp     count_ws.loop

.exit:
    ret

main:
    push    rbp
    push    rbx
    push    r12
    push    r13
    push    r14
    push    r15

    ; try to open the input file
    lea     rdi, [input_file]
    xor     rsi, rsi
    call    open_file
    cmp     rax, 0
    jge     .input_opened

    ; open failed, log a message and exit
    lea     rsi, [input_err_msg]
    mov     rdx, rax
    call    log_msg_int
    mov     rax, 1
    jmp     exit

.input_opened:
    ; the input file is open and `rax` contains the file drescriptor
    ;
    ; we mmap the input file to a random address before process it
    mov     rbx, rax
    mov     [input_fd], eax

    lea     rsi, [open_file_msg]
    mov     rdx, rbx
    call    log_msg_int

    mov     rdi, rbx
    call    file_size
    mov     [input_size], rax

    lea     rsi, [file_size_msg]
    mov     rdx, rax
    call    log_msg_int

    mov     edi, [input_fd]
    mov     rsi, [input_size]
    call    mmap_fd

    cmp     rax, 0
    jge     .input_mmaped

    ; mmap failed, log a message and exit
    lea     rsi, [mmap_err_msg]
    mov     rdx, rax
    call    log_msg_int
    mov     rax, 1
    jmp     exit

.input_mmaped:
    mov     [data_ptr], rax
    lea     rsi, [mmap_addr_msg]
    mov     rdx, rax
    call    log_msg_ptr

    mov     rdi, [data_ptr]
    call    atoi
    
    push rcx

    lea     rsi, [trace_msg]
    mov     rdx, rax
    call    log_msg_int

    pop rcx

    mov     rdi, [data_ptr]
    lea     rdi, [rdi + rcx]
    lea     rsi, [trace_msg]
    mov     rdx, rdi
    call    log_msg_ptr

    mov     rdi, [data_ptr]
    lea     rdi, [rdi + rcx]
    call    count_ws

    lea     rsi, [trace_msg]
    mov     rdx, rax
    call    log_msg_int

    xor rax, rax

exit:
    pop     r15
    pop     r14
    pop     r13
    pop     r12
    pop     rbx
    pop     rbp
    ret
