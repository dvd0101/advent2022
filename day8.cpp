#include <algorithm>
#include <fstream>
#include <iostream>
#include <ranges>
#include <vector>

#include "./generator.h"

namespace ranges = std::ranges;
namespace views = std::views;

struct tree_map {
    std::vector<int> grid;
    int32_t width = 0;
    int32_t height;
};

utils::generator<int> walk(const tree_map& tm, int x, int y, int dir_x, int dir_y) {
    x += dir_x;
    y += dir_y;
    while (x >= 0 && x < tm.width && y >= 0 && y < tm.height) {
        co_yield tm.grid[x + y * tm.width];
        x += dir_x;
        y += dir_y;
    }
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day8a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);

    tree_map tm;
    while (true) {
        char ch;
        if (!in.get(ch))
            break;

        if (ch >= '0' && ch <= '9')
            tm.grid.push_back(ch - '0');
        else if (ch == '\n' && tm.width == 0)
            tm.width = tm.grid.size();
    }

    tm.height = tm.grid.size() / tm.width;

    int num1 = 0;
    int num2 = 0;
    for (int x = 0; x < tm.width; x++) {
        for (int y = 0; y < tm.height; y++) {
            int value = tm.grid[x + y * tm.width];

            constexpr std::pair<int, int> dirs[4] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};

            for (auto [dx, dy] : dirs) {
                int visible = 1;
                for (auto x : walk(tm, x, y, dx, dy)) {
                    if (x >= value) {
                        visible = 0;
                        break;
                    }
                }

                if (visible) {
                    num1++;
                    break;
                }
            }

            int scenic = 1;
            for (auto [dx, dy] : dirs) {
                int n = 0;

                for (auto x : walk(tm, x, y, dx, dy)) {
                    n++;
                    if (x >= value)
                        break;
                }
                scenic *= n;
            }

            num2 = std::max(num2, scenic);
        }
    }

    std::cout << num1 << "\n";
    std::cout << num2 << "\n";
}
