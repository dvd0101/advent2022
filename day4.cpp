#include <fstream>
#include <iostream>

struct range {
    int low;
    int high;
};

bool contains(range a, int x) { return a.low <= x && a.high >= x; }
bool contained(range a, range b) { return b.low >= a.low && b.high <= a.high; }
bool intersects(range a, range b) {
    return contains(a, b.low) || contains(a, b.high) || contains(b, a.low) || contains(b, a.high);
}

int main(int argc, char* argv[]) {
    std::string input_name = "./inputs/day4a";
    if (argc > 1)
        input_name = argv[1];

    std::ifstream in(input_name);

    int fully_overlapped = 0;
    int overlaps = 0;
    while (in) {
        range a, b;
        char ch;
        in >> a.low >> a.high >> ch >> b.low >> b.high;
        if (!in)
            break;

        a.high *= -1;
        b.high *= -1;

        if (contained(a, b) || contained(b, a))
            fully_overlapped++;

        if (intersects(a, b))
            overlaps++;
    }

    std::cout << fully_overlapped << "\n";
    std::cout << overlaps << "\n";
}
