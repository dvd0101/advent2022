#pragma once
#include <boost/container_hash/hash.hpp>
#include <iomanip>
#include <iostream>
#include <vector>

#include "./generator.h"

namespace utils {
    struct pos {
        int x = 0;
        int y = 0;

        bool operator==(const pos&) const = default;
        auto operator<=>(const pos&) const = default;

        friend pos operator+(pos a, pos b) { return {.x = a.x + b.x, .y = a.y + b.y}; }
        friend pos& operator+=(pos& a, pos b) {
            a.x += b.x;
            a.y += b.y;
            return a;
        }
    };

    inline std::ostream& operator<<(std::ostream& os, pos pt) { return os << "(" << pt.x << "," << pt.y << ")"; }
}

template <>
struct std::hash<utils::pos> {
    std::size_t operator()(utils::pos const& p) const noexcept {
        std::size_t seed = 0;

        boost::hash_combine(seed, p.x);
        boost::hash_combine(seed, p.y);

        return seed;
    }
};

namespace utils {

    struct dir {
        int dx = 0;
        int dy = 0;
    };

    inline pos operator+(pos pt, dir d) {
        pt.x += d.dx;
        pt.y += d.dy;
        return pt;
    }

    template <typename T>
    struct grid {
        std::vector<T> cells;
        int width = 0;
        int height = 0;

        const T* lookup(pos pt) const { return lookup(posToIndex(pt)); }
        T* lookup(pos pt) { return lookup(posToIndex(pt)); }

        const T* lookup(int index) const {
            if (index < 0 || (size_t)index >= cells.size())
                return nullptr;
            return &(cells[index]);
        }

        T* lookup(int index) {
            if (index < 0 || (size_t)index >= cells.size())
                return nullptr;
            return &(cells[index]);
        }
        generator<size_t> neighbor(size_t i) const {
            auto [x, y] = indexToPos(i);
            if (x > 0)
                co_yield i - 1;
            if (x < width - 1)
                co_yield i + 1;
            if (y > 0)
                co_yield i - width;
            if (y < height - 1)
                co_yield i + width;
        }

        int posToIndex(pos pt) const { return pt.y * width + pt.x; }

        pos indexToPos(size_t i) const {
            pos pt;
            pt.y = i / width;
            pt.x = i - pt.y * width;
            return pt;
        }
    };

    template <typename T>
    std::ostream& operator<<(std::ostream& os, const grid<T>& g) {
        int i = 0;
        for (int y = 0; y < g.height; y++) {
            os << "| ";
            for (int x = 0; x < g.width; x++) {
                os << g.cells[i++] << " ";
            }
            os << "|\n";
        }
        return os;
    }

    template <typename T>
    struct sparse_grid {
        std::unordered_map<pos, T> cells;
        pos tl = {INT_MAX, INT_MAX};
        pos br = {INT_MIN, INT_MIN};

        void add(pos pt, char c) {
            cells[pt] = c;

            tl.x = std::min(tl.x, pt.x);
            tl.y = std::min(tl.y, pt.y);
            br.x = std::max(br.x, pt.x);
            br.y = std::max(br.y, pt.y);
        }
    };

    template <typename T>
    std::ostream& operator<<(std::ostream& os, const sparse_grid<T>& g) {
        os << g.tl << " " << g.br << "\n";
        for (int y = g.tl.y; y <= g.br.y; y++) {
            os << "| ";
            os << std::setw(3) << y << std::setw(0) << " ";
            for (int x = g.tl.x; x <= g.br.x; x++) {
                auto it = g.cells.find(utils::pos{x, y});
                char ch = it == g.cells.end() ? '.' : it->second;
                os << ch;
            }
            os << " |\n";
        }
        return os;
    }
}
